/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.BackStackEntry;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;

import com.example.beachmanager.R;
import com.rajesh.beachmanager.BeachManagerNucleus;
import com.rajesh.beachmanager.Constants;
import com.rajesh.beachmanager.fragments.RCPBasePagerFragment;
import com.rajesh.beachmanager.model.BeachUser;

/**
 * @author rajeshcp
 */
public class RCPPagerFragmentActivity extends FragmentActivity {


	public static final int NO_ANIMATION = -1;


	boolean mAnimationAllowded = true;

	protected boolean mPaused;


	/**
	 * @param of type null
	 * @return mAnimationAllowded of type boolean
	 * getter function for mAnimationAllowded
	 * @since Oct 27, 2013 
	 * @author rajeshcp 
	 */
	public boolean ismAnimationAllowded() {
		return mAnimationAllowded;
	}


	/**
	 * @return of type RCPPagerFragmentActivity
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public RCPPagerFragmentActivity() {
	}




	/**
	 * @param of type int 
	 * @return of type int[]
	 * function which will return the animation list 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	void setFrgamentAnimation(final FragmentTransaction ft, final int animationSetId){
		try{
			String[] animationList = getResources().getStringArray(animationSetId);
			if( animationList != null && animationList.length >= 4 ){
				ft.setCustomAnimations(getAnimationId(animationList[0]), getAnimationId(animationList[1]),
						getAnimationId(animationList[2]), getAnimationId(animationList[3]));

			}else if( animationList.length == 2 ){
				ft.setCustomAnimations(getAnimationId(animationList[0]), getAnimationId(animationList[1]));
			}
		}catch (Exception e) {
		}
	}

	/**
	 * @param name of type String 
	 * @return of type int 
	 * function which will return the identifier of the 
	 * animation
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	int getAnimationId(final String name){
		return getResources().getIdentifier(name, "anim", getPackageName());
	}

	/**
	 * @param of type null
	 * @return of type null
	 * function which will clear all the 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	void clearBackStack(){
		mAnimationAllowded = false;
		getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		mAnimationAllowded = true;
	}

	////////////////////////////////////////
	///////////////Public methods///////////
	////////////////////////////////////////

	/**
	 * @param fragmentName of type String 
	 * @param uptoLastPresence of type boolean
	 * @return of type boolean (true if popped otherwise false)
	 * function which will pop the fragment with name 
	 * @return of type 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public boolean popUntilScreen(final String fragmentClassName, final boolean uptoLastPresence){
		if( fragmentClassName == null )
			return false;
		if( fragmentClassName.length() == 0 )
			return false;
		int index = -1;

		for( int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++){
			Fragment fragment = getSupportFragmentManager().findFragmentByTag(i + "");
			if( fragment.getClass().getName().equals(fragmentClassName) ){
				index = (getSupportFragmentManager().getBackStackEntryCount() - 1) - i;
				if( !uptoLastPresence )
					break;
			}
		}

		return ( index == -1 ) ? false : popUntilScreen(index);
	}


	/**
	 * @param index of type int 
	 * @return of type boolean (true if popped else false)
	 * function which will pop the fragments until the given index reached
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public boolean popUntilScreen(final int index){
		if( getSupportFragmentManager().getBackStackEntryCount() > index ){
			BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(index);
			mAnimationAllowded = false;
			getSupportFragmentManager().popBackStackImmediate(entry.getId(), 0);
			mAnimationAllowded = true;
			return true;
		}else{
			return false;
		}
	}


	/**
	 * @param of type null
	 * @return of type RCPBasePagerFragment
	 * function which will give you the previous fragment in the queue
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public RCPBasePagerFragment getPreviousFragment(){
		if( getSupportFragmentManager() != null )
			return getFragmentAt(getSupportFragmentManager().getBackStackEntryCount() - 2);
		return null;
	}


	/**
	 * @param of type null
	 * @return of type RCPBasePagerFragment
	 * function which will get the currently shown fragment 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public RCPBasePagerFragment getFragmentAt(final int index){
		return (RCPBasePagerFragment)getSupportFragmentManager().findFragmentByTag(index + "");
	}


	/**
	 * @param fragment of type RCPBasePagerFragment
	 * @param animationSet of type int(For no animation use the NO_ANIMATION constant)
	 * Animation set should declared as an array in string.xml(provide only the name of the file with out extensions
	 * array indexing should be exactly like FrgamentTransaction.setCustomAnimations(arg0, arg1, arg2, arg3);)
	 * @param isRoot of type boolean (Provide true if you want to clear all the fragments)
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public void pushView(final RCPBasePagerFragment fragment, final int animationSet, final boolean isRoot)
	{
		if( isRoot )
			clearBackStack();
		FragmentTransaction trasaction = getSupportFragmentManager().beginTransaction();
		if( animationSet != NO_ANIMATION )
			setFrgamentAnimation(trasaction, animationSet);
		trasaction.replace(R.id.fragment_content, fragment, getSupportFragmentManager().getBackStackEntryCount() + "");
		trasaction.addToBackStack(null);
		trasaction.commit();
	}


	////////////////////////////////////////
	/////////////Override methods///////////
	////////////////////////////////////////

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onSaveInstanceState(android.os.Bundle)
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if(BeachUser.getCurrentUser() != null){
			BeachManagerNucleus.pushToLocalStorage(getApplicationContext(), Constants.USER, BeachUser.getCurrentUser().toString());
		}
		super.onSaveInstanceState(outState);
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onPause()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	protected void onPause() {
		super.onPause();
		mPaused = true;
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mPaused = false;
	}

	
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onKeyDown(int, android.view.KeyEvent)
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		RCPBasePagerFragment fragment = getFragmentAt(getSupportFragmentManager().getBackStackEntryCount() - 1);
		if( fragment != null && fragment.onKeyDown() )
			return true;
		return super.onKeyDown(keyCode, event);
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onBackPressed()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onBackPressed() {
		RCPBasePagerFragment fragment = getFragmentAt(getSupportFragmentManager().getBackStackEntryCount() - 1);
		if( fragment != null && fragment.onBackPressed() )
			return;
		if( getSupportFragmentManager().getBackStackEntryCount() > 1 ){
			super.onBackPressed();
		}else{
			finish();
		}
	}

}
