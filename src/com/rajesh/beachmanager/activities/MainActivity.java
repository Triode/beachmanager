package com.rajesh.beachmanager.activities;

import android.os.Bundle;

import com.example.beachmanager.R;
import com.rajesh.beachmanager.fragments.LauncherFragment;
import com.rajesh.beachmanager.model.BeachUser;

public class MainActivity extends RCPPagerFragmentActivity {
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if(savedInstanceState == null){
			pushView(new LauncherFragment(), R.array.slide_vertical_animation_set, true);
		}else{
			BeachUser.recreateUser(getApplicationContext());
		}
	}
}
