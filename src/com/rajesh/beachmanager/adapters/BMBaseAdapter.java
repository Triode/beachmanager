/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.adapters;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.beachmanager.R;
import com.rajesh.beachmanager.Constants;
import com.rajesh.beachmanager.model.RCPVOBase;
import com.rajesh.beachmanager.model.RCPVOFactory.FactoryBinder;

/**
 * @author rajeshcp
 */
public abstract class BMBaseAdapter extends ArrayAdapter<RCPVOBase> implements OnClickListener, FactoryBinder{

	List<RCPVOBase> mDataProvider;
	protected ListView mListView;

	/**
	 * @param mListView of type 
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public void setmListView(ListView mListView) {
		this.mListView = mListView;
	}

	static final int NORMAL_VIEW = 1;

	int mLoadState;

	Bundle mParams;
	String mAPIURL, mJSONParserMethod;

	/**
	 * @author rajeshcp
	 * holder for loading screen 
	 */
	class ProgressViewHolder{
		ProgressBar loader;
		TextView loaderText;
		Button retry;
		RelativeLayout progressHolder;
	}



	/**
	 * @param context
	 * @param textViewResourceId  
	 * @return of type HWBaseAdpater
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public BMBaseAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	/**
	 * @param context
	 * @param resource
	 * @param textViewResourceId  
	 * @return of type HWBaseAdpater
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public BMBaseAdapter(Context context, int resource, int textViewResourceId) {
		super(context, resource, textViewResourceId);
	}

	/**
	 * @param context
	 * @param textViewResourceId
	 * @param objects  
	 * @return of type HWBaseAdpater
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public BMBaseAdapter(Context context, int textViewResourceId, List<RCPVOBase> objects) {
		super(context, textViewResourceId, objects);
		mDataProvider = objects;
	}


	/**
	 * @param context
	 * @param resource
	 * @param textViewResourceId
	 * @param objects  
	 * @return of type HWBaseAdpater
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public BMBaseAdapter(Context context, int resource, int textViewResourceId,
			List<RCPVOBase> objects) {
		super(context, resource, textViewResourceId, objects);
		mDataProvider = objects;
	}

	/**
	 * @param apiURL
	 * @param params of type 
	 * @param jsonParserMethod of type String(The method to be called to parse the JSON 
	 * string to Objects inside JSONParser)
	 * function which will initiate the server API call 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public void loadServerData(String apiURL, Bundle params, String jsonParserMethod){
		mParams           = params;
		mAPIURL           = apiURL;
		mJSONParserMethod = jsonParserMethod;
		initiateBackgroundProcess();
	}



	/**
	 * @param of type null
	 * @return of type null 
	 * function which will initiate the server process
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	protected void initiateBackgroundProcess(){
//		mLoadState = Constants.STATE_LOADING;
//		BMRemoteGateway gateway = new BMRemoteGateway(mAPIURL, this, mJSONParserMethod);
//		gateway.doGet(mParams);
//		notifyDataSetChanged();
	}

	/**
	 * @param type of type int 
	 * @return of type String 
	 * function which will provide the ui notification if data getting loded 
	 * will show the user about the progess 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	protected String provideUINotification(int type){
		String notification = "";
		switch(type){
		case Constants.LOAD_ERROR :
			notification = "Some thing went wrong please try again";
			break;
		case Constants.NETWORK_ERROR :
			notification = "Make sure you have a data connection";
			break;
		case Constants.EMPTY_RESULT :
			notification = "No data available";
			break;
		case Constants.STATE_LOADING :
			notification = "Loading Data";
			break;
		}
		return notification;
	}

	/////////////////////////////////////////////////////
	/////////////////Abstract Methods////////////////////
	/////////////////////////////////////////////////////

	/**
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return of type View
	 * function which will return the view to the ListView 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	protected abstract View doGetView(int position, View convertView, ViewGroup parent);



	/////////////////////////////////////////////////////
	/////////////////Override Methods////////////////////
	/////////////////////////////////////////////////////





	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(getItemViewType(position) == IGNORE_ITEM_VIEW_TYPE){
			ProgressViewHolder holder;
			if(convertView == null){
				holder            = new ProgressViewHolder();
				convertView       = View.inflate(getContext(), R.layout.expandable_progress_layout, null);
				holder.loader     = (ProgressBar)convertView.findViewById(R.id.progressBar);
				holder.loaderText = (TextView)convertView.findViewById(R.id.progress_text);
				holder.retry      = (Button)convertView.findViewById(R.id.retry_button);
				holder.progressHolder = (RelativeLayout)convertView.findViewById(R.id.progress_holder);
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)holder.progressHolder.getLayoutParams();
				params.height = mListView.getMeasuredHeight();
				holder.progressHolder.setLayoutParams(params);

				convertView.setTag(holder);
			}else{
				holder = (ProgressViewHolder)convertView.getTag();
			}
			holder.loaderText.setText(provideUINotification(mLoadState));
			switch(mLoadState){
			case Constants.EMPTY_RESULT:{
				holder.loader.setVisibility(View.INVISIBLE);
				holder.retry.setVisibility(View.GONE);
				break;
			}
			case Constants.STATE_LOADING:{
				holder.loader.setVisibility(View.VISIBLE);
				holder.retry.setVisibility(View.GONE);
				break;
			}
			case Constants.LOAD_ERROR:{
				holder.loader.setVisibility(View.INVISIBLE);
				holder.retry.setVisibility(View.VISIBLE);
				holder.retry.setOnClickListener(this);
				break;
			}
			case Constants.NETWORK_ERROR:{
				holder.loader.setVisibility(View.INVISIBLE);
				holder.retry.setVisibility(View.VISIBLE);
				holder.retry.setOnClickListener(this);
				break;
			}
			}
			return convertView;
		}

		return doGetView(position, convertView, parent);
	}

	/* (non-Javadoc)
	 * @see android.widget.BaseAdapter#getItemViewType(int)
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public int getItemViewType(int position) {
		return (isEmpty()) ? IGNORE_ITEM_VIEW_TYPE : NORMAL_VIEW;
	}

	/* (non-Javadoc)
	 * @see android.widget.BaseAdapter#getViewTypeCount()
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public int getViewTypeCount() {
		return 2;
	}

	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getItem(int)
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public RCPVOBase getItem(int position) {
		return (mDataProvider != null && mDataProvider.size() > position) ? 
				mDataProvider.get(position) : null;
	}

	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getCount()
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public int getCount() {
		return((!isEmpty()) ? mDataProvider.size() : 1);
	}

	/* (non-Javadoc)
	 * @see android.widget.BaseAdapter#isEmpty()
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public boolean isEmpty() {
		if(mDataProvider == null)
			return true;
		return (mDataProvider.size() == 0);
	}

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.retry_button){
			initiateBackgroundProcess();
		}
	}

	

	/*
	 * (non-Javadoc)
	 * @see com.rajesh.hangoutwhere.utils.WHRemoteGateway.ServerCallback#onNetworkError()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
//	@Override
//	public void onNetworkError() {
//		mLoadState = Constants.NETWORK_ERROR;
//		notifyDataSetChanged();
//	}

	/* (non-Javadoc)
	 * @see com.rajesh.hangoutwhere.model.RCPVOFactory.FactoryBinder#freeObject()
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public void freeObject() {
		if(mDataProvider != null){
			for(RCPVOBase vo : mDataProvider){
				vo.freeObject();
			}
		}
		mDataProvider = null;
		notifyDataSetInvalidated();
	}

	/* (non-Javadoc)
	 * @see com.rajesh.hangoutwhere.model.RCPVOFactory.FactoryBinder#clearData()
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public void clearData() {
	}

}
