/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 22, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.DisplayMetrics;

/**
 * @author rajeshcp
 */
public class MathHelper {

	/**
	 * @return of type MathHelper
	 * Constructor function
	 * @since Oct 22, 2013 
	 * @author rajeshcp
	 */
	private MathHelper() {
	}



	/**
	 * @param p1 of type Point
	 * @param p2 of type Point
	 * @return of type double
	 * function which will calculate the angle between two points
	 * @since Oct 22, 2013
	 * @author rajeshcp
	 */
	public static double getAngleBetweenPoints( Point p1, Point p2 ){
		double deltaX  = p2.x - p1.x;
		double delataY = p2.y - p1.y;
		double angle   = Math.atan2(delataY, deltaX);
		return angle;
	}

	/**
	 * @param angle of type double
	 * @param p1 of type Point
	 * @return of type Point
	 * function which will find the point at distance 
	 * @since Oct 22, 2013
	 * @author rajeshcp
	 */
	public static Point getPoint(double angle, float distance, Point origin){
		origin.x = (int)(origin.x + (distance*Math.cos(angle)));
		origin.y = (int)(origin.y + (distance*Math.sin(angle)));
		return origin;
	}

	/**
	 * @param a of type Point
	 * @param b of type Point
	 * @return of type int 
	 * function which will calculate the distance between the 
	 * points
	 * @since Oct 22, 2013
	 * @author rajeshcp
	 */
	public static int getDistanceBetweenPoints(final Point a, final Point b){
		double dx   = a.x - b.x;          
		double dy   = a.y - b.y;         
		double dist = Math.sqrt( dx*dx + dy*dy ); 
		return (int)dist;
	}

	/**
	 * This method converts dp unit to equivalent pixels, depending on device density. 
	 * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent px equivalent to dp depending on device density
	 */
	public static float convertDpToPixel(float dp, Context context){
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	/**
	 * @param width if type int 
	 * @param height of type int 
	 * @return of type int 
	 * function which will calculate the diagonal 
	 * @since Oct 22, 2013
	 * @author rajeshcp
	 */
	public static int getDiagonal(final int width, final int height){
		return (int)Math.sqrt((width * width) + (height * height));	
	}


}
