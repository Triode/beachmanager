/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;

import com.rajesh.beachmanager.BeachManagerNucleus;
import com.rajesh.beachmanager.Constants;
import com.rajesh.beachmanager.model.BeachUser;

/**
 * @author rajeshcp
 */
public class BMRemoteGateway extends AsyncTask<String, Void, Integer> {

	StringBuilder mGetRequestBuilder;
	ServerCallback mCallback;
	String mParserMethod, mResponse;
	int mRequestType;


	public static final int PUT  = 20;
	public static final int GET  = 21;
	public static final int POST = 22;


	/**
	 * @param apiurl  
	 * @return of type WHRemoteGateway
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public BMRemoteGateway(String apiurl, ServerCallback mCallback, String jsonParserMethodName) {
		mGetRequestBuilder = new StringBuilder(apiurl);
		this.mCallback = mCallback;
		mParserMethod = jsonParserMethodName;
	}

	/**
	 * @param of type null 
	 * function which will perform get operation
	 * mGetRequestBuilder
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public void doGet(){
		mRequestType = GET;
		execute("");
	}

	/**
	 * @param of type null 
	 * @return of type null 
	 * function which will perform the post
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public void doPost(){
		mRequestType = POST;
		execute("");
	}

	/**
	 * @param params of type Bundle 
	 * @return of type null 
	 * function which will perform the PUT HTTP operation
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public void doPut(String params){
		mRequestType = PUT;
		execute(params);
	}


	/* (non-Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	protected Integer doInBackground(String... params) {
		HttpURLConnection conn = null;
		StringBuilder jsonResults = null;
		int respnoseCode;
		try{ 
			conn = (HttpURLConnection) new URL(mGetRequestBuilder.toString()).openConnection();
			conn.setUseCaches(false);
			conn.setRequestProperty("Accept", "*/*");
			
			String authToken = (String)BeachUser.getCurrentUser().getProperty(Constants.AUTHENTICATION_TOKEN);
			conn.addRequestProperty(Constants.AUTHORIZATION, "Token " + authToken);
			switch(mRequestType){
			case POST :
				conn.setRequestMethod("POST");
				break;
			case PUT :
				conn.setDoOutput(true);
				conn.setRequestMethod("PUT");
				if(params != null && params.length > 0 && params[0] != null){

					conn.setRequestProperty(Constants.CONTENT_TYPE, 
							Constants.CONTENT_JSON);
					OutputStream os = conn.getOutputStream();
					os.write(params[0].toString().getBytes("UTF-8"));
					os.close();
				}
				break;
			case GET :
				conn.setRequestMethod("GET");
				break;
			}
			InputStream inStream = conn.getInputStream();
			respnoseCode = conn.getResponseCode();
			if(inStream != null){
				InputStreamReader in = new InputStreamReader(inStream);
				if(respnoseCode == 200 || respnoseCode == 201 || respnoseCode == 204){
					jsonResults = new StringBuilder();
					int read;
					char[] buff = new char[1024];
					while ((read = in.read(buff)) != -1) {
						jsonResults.append(buff, 0, read);
					}
					BeachManagerNucleus.log(getClass().getName(), jsonResults.toString());
					mResponse = jsonResults.toString();
					respnoseCode = Constants.LOAD_SUCCESS;
				}
			}
		}catch(IOException ioException){
			respnoseCode = Constants.NETWORK_ERROR;
			BeachManagerNucleus.log(getClass().getName(), "Network Error");
		}catch(Exception exception){
			respnoseCode = Constants.LOAD_ERROR;
			exception.printStackTrace();
		}finally{
			if (conn != null) {
				conn.disconnect();
			}
		}
		return respnoseCode;
	}


	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		if(mCallback != null){
			if(result == Constants.LOAD_SUCCESS){
				mCallback.onSuccess(mResponse);
			}else{
				if(result == Constants.NETWORK_ERROR){
					mCallback.onNetworkError();
				}else{
					mCallback.onserverError();
				}
			}
		}
	}

	/**
	 * @author rajeshcp
	 * call back to listen for the server calls 
	 */
	public static interface ServerCallback{
		/**
		 * function which will be called on server error
		 * @since Oct 27, 2013
		 * @author rajeshcp
		 */
		public void onserverError();
		/**
		 * function which will be called on network error
		 * @since Oct 27, 2013
		 * @author rajeshcp
		 */
		public void onNetworkError();
		/**
		 * @param result of type String
		 * funcion which will be called on success of the server 
		 * request
		 * @since Oct 27, 2013
		 * @author rajeshcp
		 */
		public void onSuccess(String result);
	}
}
