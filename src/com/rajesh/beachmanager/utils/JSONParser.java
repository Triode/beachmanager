/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rajesh.beachmanager.BeachManagerNucleus;
import com.rajesh.beachmanager.Constants;
import com.rajesh.beachmanager.model.Beach;
import com.rajesh.beachmanager.model.RCPVOBase;

/**
 * @author rajeshcp
 */
public class JSONParser {

	/**
	 * @return of type JSONParser
	 * Constructor function
	 * @since Oct 23, 2013 
	 * @author rajeshcp
	 */
	private JSONParser() {
	}

	/**
	 * @param object
	 * @param keyString
	 * @return of type 
	 * @since Oct 24, 2013
	 * @author rajeshcp
	 */
	public static String getString(JSONObject object, String keyString){
		String result = "";
		try{
			result = object.getString(keyString);
		}catch(Exception e){

		}
		return result;
	}

	/**
	 * @param object
	 * @param keyString
	 * @return of type 
	 * @since Oct 24, 2013
	 * @author rajeshcp
	 */
	public static double getDouble(JSONObject object, String keyString){
		double result = 0.0f;
		try{
			result = object.getDouble(keyString);
		}catch(Exception e){

		}
		return result;
	}

	/**
	 * @param beach
	 * @return of type List<RCPVOBase>
	 * function which will return the kids list
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public static ArrayList<RCPVOBase> getKidsList(Beach beach){
		ArrayList<RCPVOBase> kidsList = null;
		try{
			JSONArray kids = (JSONArray)beach.getProperty(Constants.KIDS);
			kidsList = new ArrayList<RCPVOBase>(kids.length());
			for(int i = 0; i < kids.length(); i++){
				RCPVOBase vo = BeachManagerNucleus.mVOFactory.getObject(RCPVOBase.class.getName());
				vo.setmJsonObject(kids.getJSONObject(i));
				kidsList.add(vo);
			}
		}catch(Exception exception){
		}
		new Date().compareTo(new Date());
		Comparator<RCPVOBase> comparator = new Comparator<RCPVOBase>() {

			@Override
			public int compare(RCPVOBase arg0, RCPVOBase arg1) {
				int age0 = (Integer)arg0.getProperty(Constants.AGE);
				int age1 = (Integer)arg1.getProperty(Constants.AGE);
				return (age0 > age1 ? 1 : -1);
			}
		};
		Collections.sort(kidsList, comparator);
		return kidsList;
	}

}
