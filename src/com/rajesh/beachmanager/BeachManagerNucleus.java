/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.rajesh.beachmanager.model.RCPVOFactory;


/**
 * @author rajeshcp
 * class to hold all the singleton instances this will avoid the unwanted if condition 
 * checks in the SingleTon classes. All the properties declared as public to give access 
 * to any where form the 
 * application. 
 */
public class BeachManagerNucleus {

	static final boolean DEBUG = true;
	public static RCPVOFactory mVOFactory;
	/**
	 * @return of type HWNucleus
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	private BeachManagerNucleus() {
	}

	/**
	 * @param of type null 
	 * @return of type null
	 * function which will instantiate all related classes(factories and global instances)
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public static void chargeMe(Context applicationContext){
		mVOFactory       = RCPVOFactory.getmInstance();
	}
	
	/**
	 * @param of type null
	 * @return of type null
	 * function which will diffuse all related classes(factories and global instances)
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public static void diffuseMe(){
		mVOFactory.freePool();
	}
	
	/**
	 * @param tag of type String 
	 * @param message of type String 
	 * function which will log the messages to the logcat only 
	 * if the DEBUG is true
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public static void log(final String tag, final String message){
		if(DEBUG){
			Log.d(tag, message);
		}
	}
	
	/**
	 * @param context of type Context 
	 * @param key of type String 
	 * @param value of type String 
	 * function which will update the shared prefrence 
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public static void pushToLocalStorage(final Context context, final String key, final String value){
		SharedPreferences pref = context.getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	/**
	 * @param context of type Context 
	 * @param key of type String 
	 * @param value of type String 
	 * function which will get value from shared preference  
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public static String pullFromLocalStorage(final Context context, final String key){
		SharedPreferences pref = context.getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
		return pref.getString(key, "");
	}
	
	/**
	 * @param context of type Context 
	 * function which will clear the user name and password if it is saved   
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public static void claerUserHistory(final Context context){
		SharedPreferences pref = context.getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putString(Constants.USER_NAME, "");
		editor.putString(Constants.USER_NAME, "");
		editor.commit();
	}
	
	
}
