/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 29, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.model;

import org.json.JSONObject;

import android.graphics.Color;
import android.os.Parcel;

import com.rajesh.beachmanager.BeachManagerNucleus;
import com.rajesh.beachmanager.Constants;
import com.rajesh.beachmanager.model.RCPVOFactory.SecretInstanceClass;
import com.rajesh.beachmanager.utils.BMRemoteGateway;
import com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback;

/**
 * @author rajeshcp
 */
public class Beach extends RCPVOBase {

	/**
	 * @param secretInstance  
	 * @return of type Beach
	 * Constructor function
	 * @since Oct 29, 2013 
	 * @author rajeshcp
	 */
	public Beach(SecretInstanceClass secretInstance) {
		super(secretInstance);
	}

	/**
	 * @param in  
	 * @return of type Beach
	 * Constructor function
	 * @since Oct 29, 2013 
	 * @author rajeshcp
	 */
	public Beach(Parcel in) {
		super(in);
	}


	/**
	 * @return of type 
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public int getDirtiness(){
		return (Integer)getProperty(Constants.DIRTINESS);
	}

	public int getHappiness(){
		return (Integer)getProperty(Constants.HAPPINESS);
	}

	public String getStatus(){
		return (String)getProperty(Constants.STATE);
	}

	public String getFlag(){
		int flag = (Integer)getProperty(Constants.FLAG);
		return (flag == 0) ? Constants.GREEN : (flag == 1) ? Constants.RED : Constants.YELLOW;
	}


	public int getFlagColor(){
		int flag  = (Integer)getProperty(Constants.FLAG);
		return (flag == 0) ? Color.GREEN : (flag == 1) ? Color.RED : Color.YELLOW;
	}

	/**
	 * @param mCallBack of type BeachManagerCallBack 
	 * @return of type null
	 * function which will initialize the beach object 
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public static Beach initBeach(BeachManagerCallBack mCallBack){
		Beach beach = (Beach) BeachManagerNucleus.mVOFactory.getObject(Beach.class.getName());
		beach.inittialise(mCallBack);
		return beach;
	}


	/**
	 * @param mCallBack of type BeachManagerCallBack
	 * function which will initiate get operation of beach
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	void inittialise(final BeachManagerCallBack mCallBack){
		ServerCallback callback = new ServerCallback() {

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onserverError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onserverError() {
				mCallBack.onError("Operation failed due to server error");
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onSuccess(java.lang.String)
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onSuccess(String result) {
				try{
					Beach.this.mJsonObject = new JSONObject(result);
				}catch(Exception exception){

				}
				mCallBack.onSuccess();
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onNetworkError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onNetworkError() {
				mCallBack.onError("Make sure you have a data connection");
			}
		};
		BMRemoteGateway gateway = new BMRemoteGateway(Constants.BASE_API + Constants.BEACH_INFO, callback, "");
		gateway.doGet();
	}

	/**
	 * @param mCallback of type BeachManagerCallBack
	 * function which will initiate open beach request
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public void openBeach(final BeachManagerCallBack mCallback){
		ServerCallback callback = new ServerCallback() {

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onserverError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onserverError() {
				mCallback.onError("Operation failed due to server error");
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onSuccess(java.lang.String)
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onSuccess(String result) {
				try{
					Beach.this.mJsonObject.put(Constants.STATE, "open");
				}catch(Exception exception){
				}
				if(!mJsonObject.has(Constants.KIDS)){
					inittialise(mCallback);
				}else{
					mCallback.onSuccess();
				}
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onNetworkError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onNetworkError() {
				mCallback.onError("Make sure you have a data connection");
			}
		};
		BMRemoteGateway gateway = new BMRemoteGateway(Constants.BASE_API + Constants.OPEN_BEACH, callback, "");
		gateway.doPut(null);
	}

	/**
	 * @param mCallback of type 
	 * function which will initiate close beach request
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public void closeBeach(final BeachManagerCallBack mCallback){
		ServerCallback callback = new ServerCallback() {

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onserverError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onserverError() {
				mCallback.onError("Operation failed due to server error");
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onSuccess(java.lang.String)
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onSuccess(String result) {
				try{
					Beach.this.mJsonObject.put(Constants.STATE, "closed");
				}catch(Exception exception){
				}
				mCallback.onSuccess();
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onNetworkError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onNetworkError() {
				mCallback.onError("Make sure you have a data connection");
			}
		};
		BMRemoteGateway gateway = new BMRemoteGateway(Constants.BASE_API + Constants.CLOSE_BEACH, callback, "");
		gateway.doPut(null);
	}

	/**
	 * @param mCallback of type 
	 * function which will initiate clean beach request
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public void cleanBeach(final BeachManagerCallBack mCallback){
		ServerCallback callback = new ServerCallback() {

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onserverError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onserverError() {
				mCallback.onError("Operation failed due to server error");
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onSuccess(java.lang.String)
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onSuccess(String result) {
				try{
					Beach.this.mJsonObject.put(Constants.DIRTINESS, 0);
				}catch(Exception exception){
				}
				mCallback.onSuccess();
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onNetworkError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onNetworkError() {
				mCallback.onError("Make sure you have a data connection");
			}
		};
		BMRemoteGateway gateway = new BMRemoteGateway(Constants.BASE_API + Constants.CLEAN_BEACH, callback, "");
		gateway.doPost();
	}

	/**
	 * @param mCallback of type
	 * function which will initiate throw nivia ball beach request 
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public void throwNiveaBall(final BeachManagerCallBack mCallback){
		ServerCallback callback = new ServerCallback() {

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onserverError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onserverError() {
				mCallback.onError("Operation failed due to server error");
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onSuccess(java.lang.String)
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onSuccess(String result) {
				try{
					mJsonObject.put(Constants.HAPPINESS, 100);
				}catch(Exception e){
				}
				mCallback.onSuccess();
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onNetworkError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onNetworkError() {
				mCallback.onError("Make sure you have a data connection");
			}
		};
		BMRemoteGateway gateway = new BMRemoteGateway(Constants.BASE_API + Constants.THROW_BALL, callback, "");
		gateway.doPost();
	}

	/**
	 * @param mCallback of type 
	 * function which will initiate change flag request
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public void changeFlag(final int flag, final BeachManagerCallBack mCallback){
		ServerCallback callback = new ServerCallback() {

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onserverError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onserverError() {
				mCallback.onError("Operation failed due to server error");
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onSuccess(java.lang.String)
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onSuccess(String result) {
				try{
					mJsonObject.put(Constants.FLAG, flag);
				}catch(Exception e){

				}
				mCallback.onSuccess();
			}

			/*
			 * (non-Javadoc)
			 * @see com.rajesh.beachmanager.utils.BMRemoteGateway.ServerCallback#onNetworkError()
			 * @since Oct 29, 2013
			 * @author rajeshcp
			 */
			@Override
			public void onNetworkError() {
				mCallback.onError("Make sure you have a data connection");
			}
		};
		try{
			BMRemoteGateway gateway = new BMRemoteGateway(Constants.BASE_API + Constants.CHANGE_FLAG, callback, "");
			JSONObject putData = new JSONObject();
			putData.put(Constants.FLAG, flag);
			gateway.doPut(putData.toString());
		}catch (Exception e){
		}


	}

	/**
	 * @author rajeshcp
	 * interface to listen for the beach operations 
	 */
	public interface BeachManagerCallBack{
		/**
		 * @param of type null
		 * @return of type null 
		 * function which will be called on operation success
		 * @since Oct 29, 2013
		 * @author rajeshcp
		 */
		public void onSuccess();

		/**
		 * @param of type null 
		 * @return of type null
		 * function which will be called on operation error
		 * @since Oct 29, 2013
		 * @author rajeshcp
		 */
		public void onError(String error);
	}

}
