/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Base64;

import com.rajesh.beachmanager.BeachManagerNucleus;
import com.rajesh.beachmanager.Constants;
import com.rajesh.beachmanager.model.RCPVOFactory.SecretInstanceClass;

/**
 * @author rajeshcp
 */
public class BeachUser extends RCPVOBase {

	static BeachUser mUser;

	Bundle userData;

	/**
	 * @param of type null
	 * @return mUser of type BeachUser
	 * getter function for mUser
	 * @since Oct 27, 2013 
	 * @author rajeshcp 
	 */
	public static BeachUser getCurrentUser() {
		return mUser;
	}


	/**
	 * @param secretInstance  
	 * @return of type BeachUser
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public BeachUser(SecretInstanceClass secretInstance) {
		super(secretInstance);
		userData = new Bundle();
	}

	/**
	 * @param in  
	 * @return of type BeachUser
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public BeachUser(Parcel in) {
		super(in);
	}

	/**
	 * @param userName of type String
	 * @param password of type String 
	 * @return of type String 
	 * function which will return the post user data JSON String 
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	static String getUserPostData(final String userName, final String password){
		String result = "";
		try{
			JSONObject parent = new JSONObject();
			JSONObject child  = new JSONObject();
			child.put(Constants.USER_NAME, userName);
			child.put(Constants.PASSWORD, password);
			parent.put(Constants.USER, child);
			result = parent.toString();
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}


	/**
	 * @param username of type 
	 * setter function for user name 
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	public void setUserName(final String username){
		userData.putString(Constants.USER_NAME, username);
	}

	/**
	 * @param username of type 
	 * setter function for password 
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	public void setUserPassword(final String password){
		userData.putString(Constants.PASSWORD, password);
	}

	/**
	 * @param userName of type String
	 * @param password of type String 
	 * function which will perform the login for the user
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public static void login(final String userName, final String password, final SigninCallBack callback){
		try{
			String authString = userName + ":" + password;
			authString = Base64.encodeToString(authString.getBytes("UTF-8"), Base64.NO_WRAP);
			String authStringEnc = new String(authString);
			new SignUpLoginTask(callback, false).execute(authStringEnc);
		}catch(Exception e){

		}

	}

	/**
	 * @return of type null 
	 * @param conext of type Context
	 * to re create the user in case of app force close 
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	public static void recreateUser(Context conext){
		String userJson = BeachManagerNucleus.pullFromLocalStorage(conext, Constants.USER);
		if(userJson.length() > 0){
			try{
				JSONObject object = new JSONObject(userJson);
				BeachUser user = (BeachUser)BeachManagerNucleus.mVOFactory.getObject(BeachUser.class.getName());
				user.setmJsonObject(object);
				mUser = user;
			}catch(Exception exception){
			}
		}
	}


	/**
	 * @param userName of type String
	 * @param password of type String 
	 * @param of type SigninCallBack 
	 * @return of type null 
	 * function which will perform the sign up if the user name and password is set 
	 * throw 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public static void signUp(final String userName, final String password, final SigninCallBack callback){
		new SignUpLoginTask(callback, true).execute(getUserPostData(userName, password));
	}


	/**
	 * @author rajeshcp
	 */
	private static class SignUpLoginTask extends AsyncTask<String, Void, String>{



		String mURL, mType;
		SigninCallBack mSignInCallback;
		/**
		 * @param callback  
		 * @return of type SignUpLoginTask
		 * Constructor function
		 * @since Oct 28, 2013 
		 * @author rajeshcp
		 */
		public SignUpLoginTask(SigninCallBack callback, final boolean isSignUP){
			mSignInCallback = callback;
			mType = ((isSignUP) ? Constants.SIGN_UP : Constants.SIGN_IN);
			mURL  = Constants.BASE_API + mType;
		}

		/*
		 * (non-Javadoc)
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 * @since Oct 28, 2013
		 * @author rajeshcp
		 */
		@Override
		protected String doInBackground(final String... params) {
			String result = "";
			try{
				HttpURLConnection connection;
				if(mType.equals(Constants.SIGN_UP)){
					connection =  (HttpURLConnection)new 
							URL(mURL).openConnection();
					connection.setRequestMethod("POST");
					connection.setDoOutput(true);
					connection.setDoOutput(true);
					connection.setRequestProperty(Constants.CONTENT_TYPE, 
							Constants.CONTENT_JSON);
					connection.setRequestProperty("Accept", "*/*");
					connection.connect();
					OutputStream os = connection.getOutputStream();
					os.write(params[0].toString().getBytes("UTF-8"));
					os.close();
				}else{
					connection = (HttpURLConnection)new 
							URL(mURL).openConnection();
					connection.setRequestMethod("GET");

					connection.setUseCaches(false);
					connection.setRequestProperty("Accept", "*/*");
					connection.addRequestProperty("Authorization", "Basic " + params[0]);
				}

				InputStream in = connection.getInputStream();
				if(in != null){
					StringBuilder resultBuilder = new StringBuilder();
					InputStreamReader reader = new 
							InputStreamReader(in);
					int read;
					char[] buff = new char[1024];
					while ((read = reader.read(buff)) != -1) {
						resultBuilder.append(buff, 0, read);
					}
					result = resultBuilder.toString();
				}
			}catch(IOException exception){
				//exception.
				return "IOException";
			}
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 * @since Oct 28, 2013
		 * @author rajeshcp
		 */
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(result.equals("IOException")){
				mSignInCallback.onNetworkError();
			}else{
				try{
					JSONObject object = new JSONObject(result);
					BeachUser user = (BeachUser)BeachManagerNucleus.mVOFactory.getObject(BeachUser.class.getName());
					user.setmJsonObject(object);
					mUser = user;
					mSignInCallback.onSignin(user, null);
				}catch(Exception exception){
					mSignInCallback.onSignin(null, new UserException(result, -1));
				}
			}
		}
	}



	/**
	 * @author rajeshcp
	 * call to listen for sign up and login
	 */
	public interface SigninCallBack{
		/**
		 * @param user of type BeachUser
		 * @param exception of type SignUpException
		 * function which will be called on SignUp
		 * @since Oct 27, 2013
		 * @author rajeshcp
		 */
		public void onSignin(BeachUser user, UserException exception);

		/**
		 * @param of type null 
		 * @return of type null
		 * function which will be called on network error
		 * @since Oct 27, 2013
		 * @author rajeshcp
		 */
		public void onNetworkError();
	}


	/**
	 * @author rajeshcp
	 */
	public static class UserException extends Exception{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private int mErrorCode;

		/**
		 * @param of type null
		 * @return mErrorCode of type int
		 * getter function for mErrorCode
		 * @since Oct 28, 2013 
		 * @author rajeshcp 
		 */
		public int getmErrorCode() {
			return mErrorCode;
		}

		/**
		 * @return of type SignUpException
		 * Constructor function
		 * @since Oct 27, 2013 
		 * @author rajeshcp
		 */
		public UserException(String message, int errorCode) {
			super(message);
			mErrorCode = errorCode;
		}
	}


}
