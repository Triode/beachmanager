/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author rajeshcp
 */
public class RCPVOFactory {



	private static RCPVOFactory mInstance;

	/**
	 * @param of type null
	 * @return mInstance of type RCPVOFactory
	 * getter function for mInstance
	 * @since Oct 27, 2013 
	 * @author rajeshcp 
	 */
	public static RCPVOFactory getmInstance() {
		mInstance = ( mInstance == null ) ? new RCPVOFactory( new SingleTonEnforcer() ) : mInstance;
		return mInstance;
	}

	///////////////////////////////////////////////////////
	/////////////////////Private Members///////////////////
	///////////////////////////////////////////////////////
	private HashMap<String, ArrayList<RCPVOBase>> mObjectPool;


	/**
	 * @return of type RCPVOFactory
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public RCPVOFactory( SingleTonEnforcer singleTon ) {
		mObjectPool = new HashMap<String, ArrayList<RCPVOBase>>();
	}


	///////////////////////////////////////////////////////
	/////////////////////private Methods///////////////////
	///////////////////////////////////////////////////////

	/**
	 * @param mType of type String
	 * @return of type null
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	private RCPVOBase fetchFrommPool( String mType )
	{
		RCPVOBase object = ( mObjectPool.get(mType) != null && mObjectPool.get(mType).size() > 0 ) ? mObjectPool.get(mType).remove(0) : null;
		return object;
	}


	///////////////////////////////////////////////////////
	//////////////////////Public/Methods///////////////////
	///////////////////////////////////////////////////////


	/**
	 * @param object of type RCPVOBase
	 * @return of type null
	 * function which will push the object to the mObjectPool
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public void freeObject(RCPVOBase object){
		mObjectPool.put(object.getmType(), ( mObjectPool.get(object.getmType()) == null ) ? new ArrayList<RCPVOBase>() : mObjectPool.get(object.getmType()));
		mObjectPool.get(object.getmType()).add(object);
	}

	/**
	 * @param of type null
	 * @return of type null
	 * function which will clear the entire objectPool
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public void freePool(){
		for( String key : mObjectPool.keySet() ){
			mObjectPool.get(key).clear();
		}
		mObjectPool.clear();
	}

	/**
	 * @param mType of type String 
	 * @return of type RCPVOBase
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public RCPVOBase getObject( String mType ){
		Object object = fetchFrommPool(mType);
		if( object == null ){
			try {
				object = Class.forName(mType).getConstructor(SecretInstanceClass.class).newInstance(new SecretInstanceClass());
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
		return (RCPVOBase)object;
	}

	/**
	 * @author rajeshcp
	 * Class to provide single ton behavior for the 
	 * parent factory
	 */
	static class SingleTonEnforcer{
	}

	/**
	 * @author rajeshcp
	 * Class to provide single ton behavior for the 
	 * parent factory
	 */
	static class SecretInstanceClass{
	}
	
	/**
	 * @author rajeshcp
	 */
	public static interface FactoryBinder{
		/**
		 * @param of type null
		 * @return of type null
		 * function which will reset the member values
		 * @since Oct 27, 2013
		 * @author rajeshcp
		 */
		public void freeObject();
		
		/**
		 * @param of type null
		 * @return of type null
		 * function which will clear the Object
		 * @since Oct 27, 2013
		 * @author rajeshcp
		 */
		public void clearData();
	}

}
