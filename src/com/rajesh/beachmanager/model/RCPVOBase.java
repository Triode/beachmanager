/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.model;

import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

import com.rajesh.beachmanager.model.RCPVOFactory.FactoryBinder;
import com.rajesh.beachmanager.model.RCPVOFactory.SecretInstanceClass;

/**
 * @author rajeshcp
 */
public class RCPVOBase implements FactoryBinder, Parcelable {


	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		/*
		 * (non-Javadoc)
		 * @see android.os.Parcelable.Creator#createFromParcel(android.os.Parcel)
		 * @since Oct 27, 2013
		 * @author rajeshcp
		 */
		public RCPVOBase createFromParcel(Parcel in) {
			return new RCPVOBase(in); 
		}

		/*
		 * (non-Javadoc)
		 * @see android.os.Parcelable.Creator#newArray(int)
		 * @since Oct 27, 2013
		 * @author rajeshcp
		 */
		public RCPVOBase[] newArray(int size) {
			return new RCPVOBase[size];
		}
	};


	static final String EMPTY_STRING           = "empty_string";
	public static final  String INVALID_RESULT = "invalid_result";

	///////////////////////////////////////////////////////
	/////////////////////Private Members///////////////////
	///////////////////////////////////////////////////////
	private String mType;
	protected JSONObject mJsonObject;




	/**
	 * @param of type null
	 * @return mJsonObject of type JSONObject
	 * getter function for mJsonObject
	 * @since Oct 27, 2013 
	 * @author rajeshcp 
	 */
	public JSONObject getmJsonObject() {
		return mJsonObject;
	}


	/**
	 * @param mJsonObject of type JSONObject
	 * @return of type null
	 * setter function for mJsonObject
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	public void setmJsonObject(JSONObject mJsonObject) {
		this.mJsonObject = mJsonObject;
	}

	/**
	 * @param of type null
	 * @return mType of type String
	 * getter function for mType
	 * @since Oct 27, 2013 
	 * @author rajeshcp 
	 */
	public String getmType() {
		return mType;
	}

	/**
	 * @param secretInstance  
	 * @return of type RCPVOBase
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public RCPVOBase( SecretInstanceClass secretInstance ) {
		mType = getClass().getName();
	}

	///////////////////////////////////////////////////////
	//////////////////////Public Methods///////////////////
	///////////////////////////////////////////////////////

	/**
	 * @param in  
	 * @return of type RCPVOBase
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public RCPVOBase(Parcel in) {
		String jsonString = in.readString();
		if(!jsonString.equals(EMPTY_STRING)){
			try{
				mJsonObject = new JSONObject(jsonString);
			}catch(Exception exception){
				exception.printStackTrace();
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * @see com.rajesh.hangoutwhere.model.RCPVOFactory.FactoryBinder#freeObject()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public void freeObject(){
		RCPVOFactory.getmInstance().freeObject(this);
		mJsonObject = null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	@Override
	public String toString() {
		return (mJsonObject != null) ? mJsonObject.toString() : super.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see com.rajesh.hangoutwhere.model.RCPVOFactory.FactoryBinder#clearData()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public void clearData(){
		mJsonObject = null;
	}


	/* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public int describeContents() {
		return 0;
	}


	/* (non-Javadoc)
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		String toWrite = EMPTY_STRING;
		if(mJsonObject != null){
			toWrite = mJsonObject.toString();
		}
		dest.writeString(toWrite);
	}

	/**
	 * @param key of type String 
	 * @return of type Object 
	 * function which will get the property from mJsonObject
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public Object getProperty(String key){
		Object result = INVALID_RESULT;
		if(mJsonObject != null && mJsonObject.has(key)){
			try{
				result = mJsonObject.get(key);
			}catch(Exception exception){

			}
		}
		return result;
	}

}
