/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager;

import android.app.Application;

/**
 * @author rajeshcp
 */
public class BeachManager extends Application {

	/**
	 * @return of type BeachManager
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public BeachManager() {
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Application#onCreate()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		BeachManagerNucleus.chargeMe(getApplicationContext());
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Application#onTerminate()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onTerminate() {
		super.onTerminate();
		BeachManagerNucleus.diffuseMe();
	}
}
