/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 28, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.beachmanager.R;
import com.rajesh.beachmanager.BeachManagerNucleus;
import com.rajesh.beachmanager.Constants;
import com.rajesh.beachmanager.model.BeachUser;
import com.rajesh.beachmanager.model.BeachUser.SigninCallBack;
import com.rajesh.beachmanager.model.BeachUser.UserException;

/**
 * @author rajeshcp
 */
public class SignInSignUpFragment extends RCPBasePagerFragment implements OnClickListener, SigninCallBack {

	Button mSubmitButton;
	int mMode;

	/**
	 * @return of type SignInSignUpFragment
	 * Constructor function
	 * @since Oct 28, 2013 
	 * @author rajeshcp
	 */
	public SignInSignUpFragment() {
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		createViewIfNecessory(R.layout.login_signup_fragment, inflater);
		super.onCreateView(inflater, container, savedInstanceState);
		return mView;
	}

	/*
	 * (non-Javadoc)
	 * @see com.rajesh.beachmanager.fragments.RCPBasePagerFragment#wireUIElements()
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	@Override
	protected void wireUIElements() {
		super.wireUIElements();
		mSubmitButton.setOnClickListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see com.rajesh.beachmanager.fragments.RCPBasePagerFragment#prepareView()
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	@Override
	protected void prepareView() {
		super.prepareView();
		mSubmitButton = (Button)mView.findViewById(R.id.submit);
		mMode         = getArguments().getInt(Constants.MODE);
		String text;
		if(mMode == R.id.sign_in){
			String username = BeachManagerNucleus.pullFromLocalStorage(getActivity().getApplicationContext(), Constants.USER_NAME);
			String pswd = BeachManagerNucleus.pullFromLocalStorage(getActivity().getApplicationContext(), Constants.PASSWORD);
			
			if(!username.isEmpty() && !pswd.isEmpty()){
				((TextView)mView.findViewById(R.id.username)).setText(username);
				((TextView)mView.findViewById(R.id.password)).setText(pswd);
			}
			text = getResources().getString(R.string.signin);
			mView.findViewById(R.id.re_password).setVisibility(View.GONE);
		}else{
			text = getResources().getString(R.string.signup);
			mView.findViewById(R.id.remember_me).setVisibility(View.GONE);
		}
		mSubmitButton.setText(text); 
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onClick(View arg0) {
		String userName = ((TextView)mView.findViewById(R.id.username)).getText().toString();
		String pswd     = ((TextView)mView.findViewById(R.id.password)).getText().toString();
		String rePswd   = ((TextView)mView.findViewById(R.id.re_password)).getText().toString();
		if(userName.length() == 0 || pswd.length() == 0){
			showToast("username/password canot be blank");
			return;
		}

		if(mMode == R.id.sign_up){
			if(!pswd.equals(rePswd)){
				showToast("Passwords doesn't match");
				return;
			}
			showProgresDialog("Creating Your Account...");
			BeachUser.signUp(userName, pswd, this);
		}else{
			showProgresDialog("Signing In...");
			BeachUser.login(userName, pswd, this);
		}
		hideSoftKeyBoard();
	}


	/*
	 * (non-Javadoc)
	 * @see com.rajesh.beachmanager.model.BeachUser.SigninCallBack#onSignin
	 * (com.rajesh.beachmanager.model.BeachUser, com.rajesh.beachmanager.model.BeachUser.UserException)
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onSignin(BeachUser user, UserException exception) {
		dismissDialog();
		if(user != null){
			if(mMode == R.id.sign_in){
				if(((CheckBox)mView.findViewById(R.id.remember_me)).isChecked()){
					String userName = ((TextView)mView.findViewById(R.id.username)).getText().toString();
					String pswd     = ((TextView)mView.findViewById(R.id.password)).getText().toString();
					BeachManagerNucleus.pushToLocalStorage(getActivity().getApplicationContext(), Constants.USER_NAME, userName);
					BeachManagerNucleus.pushToLocalStorage(getActivity().getApplicationContext(), Constants.PASSWORD, pswd);
				}
			}else{
				BeachManagerNucleus.claerUserHistory(getActivity().getApplicationContext());
			}
			
			pushView(new ManageBeachFragment(), R.array.slide_horizontal_animation_set, true);
		}else{
			
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.rajesh.beachmanager.model.BeachUser.SigninCallBack#onNetworkError()
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onNetworkError() {
		dismissDialog();
		showToast("Network lost please try again");
	}

}
