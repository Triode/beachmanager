/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 28, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.example.beachmanager.R;
import com.rajesh.beachmanager.Constants;


/**
 * @author rajeshcp
 */
public class LauncherFragment extends RCPBasePagerFragment implements OnClickListener{

	/**
	 * @return of type LauncherFragment
	 * Constructor function
	 * @since Oct 28, 2013 
	 * @author rajeshcp
	 */
	public LauncherFragment() {
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		createViewIfNecessory(R.layout.launch_fragment, inflater);
		super.onCreateView(inflater, container, savedInstanceState);
		return mView;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.rajesh.beachmanager.fragments.RCPBasePagerFragment#wireUIElements()
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	@Override
	protected void wireUIElements() {
		super.wireUIElements();
		mView.findViewById(R.id.sign_in).setOnClickListener(this);
		mView.findViewById(R.id.sign_up).setOnClickListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 * @since Oct 28, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onClick(View view) {

		Bundle data = new Bundle();
		data.putInt(Constants.MODE, view.getId());
		SignInSignUpFragment fragment = new SignInSignUpFragment();
		fragment.setArguments(data);
		pushView(fragment, R.array.slide_horizontal_animation_set, false);
	}

}
