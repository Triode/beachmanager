/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 28, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.fragments.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;

import com.example.beachmanager.R;
import com.rajesh.beachmanager.Constants;
import com.rajesh.beachmanager.activities.RCPPagerFragmentActivity;
import com.rajesh.beachmanager.fragments.SignInSignUpFragment;


/**
 * @author rajeshcp
 */
public class OptionDialog extends DialogFragment implements OnClickListener{

	/**
	 * @return of type OptionDialog
	 * Constructor function
	 * @since Oct 28, 2013 
	 * @author rajeshcp
	 */
	public OptionDialog() {
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.DialogFragment#onCreateDialog(android.os.Bundle)
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = new Dialog(getActivity(), R.style.choose_dialog_theme);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		return dialog;
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.launch_fragment, container);
		view.findViewById(R.id.sign_in).setOnClickListener(this);
		view.findViewById(R.id.sign_up).setOnClickListener(this);
		return view;
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onClick(View view) {
		dismiss();
		Bundle data = new Bundle();
		data.putInt(Constants.MODE, view.getId());
		SignInSignUpFragment fragment = new SignInSignUpFragment();
		fragment.setArguments(data);
		((RCPPagerFragmentActivity)getActivity()).pushView(fragment, R.array.slide_vertical_animation_set, true);
	}

}
