/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 29, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager.fragments;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.beachmanager.R;
import com.rajesh.beachmanager.Constants;
import com.rajesh.beachmanager.model.Beach;
import com.rajesh.beachmanager.model.Beach.BeachManagerCallBack;
import com.rajesh.beachmanager.model.RCPVOBase;
import com.rajesh.beachmanager.utils.JSONParser;

/**
 * @author rajeshcp
 */
public class ManageBeachFragment extends RCPBasePagerFragment implements BeachManagerCallBack, OnClickListener{

	Beach mBeach;
	View mBeachStatusView, mBeachFlagView, mBeachDirtinessView,
	mBeachHappinessView, mThrowNiveaBallView, mCleanBeachView, 
	mBeachDetailsContainer, mRetry, mKidsRow;


	/**
	 * @return of type ManageBeachFragment
	 * Constructor function
	 * @since Oct 29, 2013 
	 * @author rajeshcp
	 */
	public ManageBeachFragment() {

	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#
	 * onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		createViewIfNecessory(R.layout.manage_beach_fragment, inflater);
		super.onCreateView(inflater, container, savedInstanceState);
		return mView;
	}

	/*
	 * (non-Javadoc)
	 * @see com.rajesh.beachmanager.fragments.RCPBasePagerFragment#wireUIElements()
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	@Override
	protected void wireUIElements() {
		super.wireUIElements();
		mRetry.setOnClickListener(this);
		mCleanBeachView.setOnClickListener(this);
		mBeachFlagView.setOnClickListener(this);
		mBeachStatusView.setOnClickListener(this);
		mThrowNiveaBallView.setOnClickListener(this);
		mKidsRow.setOnClickListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see com.rajesh.beachmanager.fragments.RCPBasePagerFragment#prepareView()
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	@Override
	protected void prepareView() {
		super.prepareView();
		mKidsRow               = mView.findViewById(R.id.search_kids_row);
		mBeachDetailsContainer = mView.findViewById(R.id.beach_details);
		mCleanBeachView        = mView.findViewById(R.id.clean_beach);
		mBeachDirtinessView    = mView.findViewById(R.id.dirtiness_row);
		mBeachFlagView         = mView.findViewById(R.id.flag_row);
		mBeachHappinessView    = mView.findViewById(R.id.happiness_row);
		mBeachStatusView       = mView.findViewById(R.id.beach_status);
		mThrowNiveaBallView    = mView.findViewById(R.id.increase_happiness);
		mRetry                 = mView.findViewById(R.id.submit);
		mBeachDetailsContainer.setVisibility(View.GONE);
		showProgresDialog("Loading Beach Info");


		mBeach = Beach.initBeach(this);
	}

	/* (non-Javadoc)
	 * @see com.rajesh.beachmanager.model.Beach.BeachManagerCallBack#onSuccess()
	 * @since Oct 29, 2013
	 * @author rajeshcp 
	 */
	@Override
	public void onSuccess() {
		dismissDialog();
		String status = (String)mBeach.getProperty(Constants.STATE);
		((TextView)mCleanBeachView.findViewById(R.id.action_text)).setText("Clean Beach");
		if(status.equals("open")){

			mCleanBeachView.setVisibility(View.GONE);
			mBeachDirtinessView.setVisibility(View.VISIBLE);
			mBeachFlagView.setVisibility(View.VISIBLE);
			mBeachHappinessView.setVisibility(View.VISIBLE);
			mThrowNiveaBallView.setVisibility(View.VISIBLE);
			mKidsRow.setVisibility(View.VISIBLE);


			((TextView)mBeachStatusView.findViewById(R.id.action_text)).setText("Close Beach");

			((TextView)mBeachDirtinessView.findViewById(R.id.action_text)).setText("Dirtiness (" + mBeach.getDirtiness() + ")");

			((TextView)mBeachFlagView.findViewById(R.id.action_text)).setText("Change Flag");

			mBeachFlagView.findViewById(R.id.flag).setVisibility(View.VISIBLE);
			mBeachFlagView.findViewById(R.id.flag).setBackgroundColor(mBeach.getFlagColor());


			((TextView)mBeachHappinessView.findViewById(R.id.action_text)).setText("Happiness (" + mBeach.getHappiness() + ")");

			((TextView)mKidsRow.findViewById(R.id.action_text)).setText("Serach Lost Kids");

			((TextView)mThrowNiveaBallView.findViewById(R.id.action_text)).setText("Throw Ball");
		}else{
			((TextView)mBeachStatusView.findViewById(R.id.action_text)).setText("Open Beach");
			mCleanBeachView.setVisibility(View.VISIBLE);

			mBeachDirtinessView.setVisibility(View.GONE);
			mBeachFlagView.setVisibility(View.GONE);
			mBeachHappinessView.setVisibility(View.GONE);
			mKidsRow.setVisibility(View.GONE);
			mThrowNiveaBallView.setVisibility(View.GONE);
		}
		mBeachDetailsContainer.setVisibility(View.VISIBLE);
	}

	/* (non-Javadoc)
	 * @see com.rajesh.beachmanager.model.Beach.BeachManagerCallBack#onError()
	 * @since Oct 29, 2013
	 * @author rajeshcp 
	 */
	@Override
	public void onError(String error) {
		mRetry.setVisibility(View.VISIBLE);
		if(mBeach.getProperty(Constants.STATE).equals(RCPVOBase.INVALID_RESULT)){
			//Object not fetched yet
			mBeach.freeObject();
			mBeach = null;
		}
		showToast(error);
		dismissDialog();
	}

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 * @since Oct 29, 2013
	 * @author rajeshcp 
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:
			showProgresDialog("Fetching Beach Info");
			mRetry.setVisibility(View.GONE);
			mBeach = Beach.initBeach(this);
			break;
		case R.id.clean_beach:
			showProgresDialog("Cleaning beach");
			mBeach.cleanBeach(this);
			break;
		case R.id.flag_row:

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Select Flag")
			.setItems(R.array.colors_array, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					showProgresDialog("Changing Flag");
					mBeach.changeFlag(which, ManageBeachFragment.this);
				}
			});
			builder.create().show();
			break;
		case R.id.beach_status:
			String message = "";
			if(mBeach.getStatus().equals("open")){
				message = "Closing Beach";
				mBeach.closeBeach(this);
			}else{
				message = "Opening Beach";
				mBeach.openBeach(this);
			}
			showProgresDialog(message);
			break;
		case R.id.increase_happiness:
			showProgresDialog("Updating Beach Info");
			mBeach.throwNiveaBall(this);
			break;
		case R.id.search_kids_row:
			ArrayList<RCPVOBase> kids = JSONParser.getKidsList(mBeach);
			if(kids != null && kids.size() > 0){
				SearchLostKidsFrgment fragment = new SearchLostKidsFrgment();
				Bundle arguments = new Bundle();
				arguments.putParcelableArrayList(Constants.DATA, kids);
				fragment.setArguments(arguments);
				pushView(fragment, R.array.slide_horizontal_animation_set, false);
			}else{
				showToast("No Lost Kids In This Beach");
			}
			break;
		default:
			break;
		}
	}
}
