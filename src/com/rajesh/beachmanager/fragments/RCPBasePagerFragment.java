package com.rajesh.beachmanager.fragments;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.rajesh.beachmanager.Constants;
import com.rajesh.beachmanager.activities.RCPPagerFragmentActivity;

/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
/**
 * @author rajeshcp
 */
public class RCPBasePagerFragment extends Fragment {


	ProgressDialog mProgressDialog;

	ArrayList<Toast> mPendingToast;

	protected boolean mPaused, mViewCached;
	protected View mView;

	private Context mContext;

	Bundle savedInstanceState;



	/**
	 * @param of type null
	 * @return mContext of type Context
	 * getter function for mContext
	 * @since Oct 27, 2013 
	 * @author rajeshcp 
	 */
	public Context getmContext() {
		return mContext;
	}

	/**
	 * @param mContext of type Context
	 * @return of type null
	 * setter function for mContext
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	public void setmContext(Context mContext) {
		this.mContext = mContext;
	}

	/**
	 * @param of type null
	 * @return of type boolean 
	 * getter function for mViewCached
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public boolean isViewCached(){
		mViewCached = mView != null;
		return mViewCached;
	}

	/**
	 * @return of type IQBasePagerFragment
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	public RCPBasePagerFragment() {
	}

	/**
	 * @param message of type String 
	 * @return of type null 
	 * function which will show the peogress dialog
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	protected void showProgresDialog(String message){
		if(!mPaused){
			mProgressDialog = (mProgressDialog == null) ? new ProgressDialog(getActivity()) : mProgressDialog;
			mProgressDialog.setCancelable(false);
			mProgressDialog.setTitle("Progress");
			mProgressDialog.setMessage(message);
			mProgressDialog.show();
		}
	}
	
	/**
	 * @param of type null 
	 * @return of type null
	 * function which will dismiss the dialog 
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	protected void dismissDialog(){
		mProgressDialog.dismiss();
	}


	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onSaveInstanceState(android.os.Bundle)
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putBundle(Constants.DATA, getArguments());
		super.onSaveInstanceState(outState);
	}


	////////////////////////////////////////
	////////////Public methods//////////////
	////////////////////////////////////////

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 * @since Oct 27, 2013
	 * @author rajeshcp 
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	

	/**
	 * @param fragment of type RCPBasePagerFragment
	 * @param animationSet of type int
	 * @param isRoot of type boolean 
	 * function which will push the fragment 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public void pushView(final RCPBasePagerFragment fragment, final int animationSet, final boolean isRoot){
		hideSoftKeyBoard();
		((RCPPagerFragmentActivity)getActivity()).pushView(fragment, animationSet, isRoot);
	}

	/**
	 * @param of type null
	 * @return of type boolean
	 * function which will be called on user back press
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public boolean onBackPressed(){
		return false;
	}

	/**
	 * @param of type null 
	 * @return of type boolean
	 * function will be called on key down of activity 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public boolean onKeyDown(){
		return false;
	}


	/**
	 * @param of type null
	 * @return of type null
	 * function which will clear the data associated with the 
	 * fragment 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public void freeMe(){
		mView           = null;
		if( mPendingToast != null ){
			mPendingToast.clear();
			mPendingToast = null;
		}
	}

	////////////////////////////////////////
	////////////Protected methods///////////
	////////////////////////////////////////


	/**
	 * @param of type null 
	 * @return of type null 
	 * function which will hide the soft keyboard
	 * @since Oct 29, 2013
	 * @author rajeshcp
	 */
	protected void hideSoftKeyBoard(){
		final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
	}
	
	

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateAnimation(int, boolean, int)
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {

		if( !((RCPPagerFragmentActivity)getActivity()).ismAnimationAllowded() ){
			Animation a = new Animation() {};
			a.setDuration(0);
			return a;
		}

		return super.onCreateAnimation(transit, enter, nextAnim);
	}


	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if( !mViewCached ){
			prepareView();
			wireUIElements();
		}
	}



	/**
	 * @param of type null
	 * @return of type null
	 * function which will be used to set initial values 
	 * for the view 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	protected void prepareView(){
	}

	/**
	 * @param of type null
	 * @return of type null
	 * function which will add all the event listeners 
	 * for the view 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	protected void wireUIElements(){
	}

	/**
	 * @param of type Object 
	 * @return of type null
	 * function which will be called if any arguments passed 
	 * from the previous fragment 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	protected void onForwardParam(final Object params){

	}


	/**
	 * @param message of type String
	 * @return of type null
	 * function which will show the toast message 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	public void showToast(final String message){
		Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
		if( !mPaused ){
			toast.show();
		}else{
			mPendingToast = ( mPendingToast == null ) ? new ArrayList<Toast>() : mPendingToast;
			mPendingToast.add(0, toast);
		}
	}


	/**
	 * @param id of type int
	 * @param inflater LayoutInflater
	 * @return of type null
	 * function which will create the view 
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	protected void createViewIfNecessory(int id, LayoutInflater inflater){
		mView = ( isViewCached() ) ? mView : inflater.inflate(id, null);
		if( mView.getParent() != null ){
			((ViewGroup)mView.getParent()).removeAllViews();
		}
	}


	////////////////////////////////////////
	////////////Override methods////////////
	////////////////////////////////////////

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onDestroy()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(getClass().getName(), getClass().getName() + " Destroyed");
		freeMe();
	}


	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onResume()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onResume() {
		super.onResume();
		if( mPendingToast != null && mPendingToast.size() > 0 ){
			for( Toast toast : mPendingToast ){
				toast.show();
			}
			mPendingToast.clear();
		}

		mPaused = false;
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onPause()
	 * @since Oct 27, 2013
	 * @author rajeshcp
	 */
	@Override
	public void onPause() {
		super.onPause();
		mPaused = true;
	}

}
