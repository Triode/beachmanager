/*
 * Copyright (c) 2013 Rajesh CP
 * All Rights Reserved.
 * @since Oct 27, 2013 
 * @author rajeshcp
 */
package com.rajesh.beachmanager;
/**
 * @author rajeshcp
 */
public class Constants {


	public static final int LOAD_SUCCESS  = 77;
	public static final int LOAD_ERROR    = 78;
	public static final int EMPTY_RESULT  = 79;
	public static final int NETWORK_ERROR = 80;
	public static final int STATE_LOADING = 81;


	public static final String CONTENT_TYPE   = "Content-Type";
	public static final String CONTENT_JSON   = "application/json";



	public static final String BASE_API    = "http://lafosca-beach.herokuapp.com/api/v1";
	public static final String SIGN_UP     = "/users";
	public static final String SIGN_IN     = "/user";


	public static final String BEACH_INFO  = "/state";
	public static final String OPEN_BEACH  = "/open";
	public static final String CLOSE_BEACH = "/close";
	public static final String CLEAN_BEACH = "/clean";
	public static final String THROW_BALL  = "/nivea-rain";
	public static final String CHANGE_FLAG = "/flag";


	public static final String NAME                  = "name";
	public static final String AGE                   = "age";
	public static final String USER                  = "user";
	public static final String USER_NAME             = "username";
	public static final String PASSWORD              = "password";
	public static final String AUTHENTICATION_TOKEN  = "authentication_token";

	public static final String AUTHORIZATION   = "Authorization";


	public static final String APP_PREF   = "app_pref";
	public static final String DATA       = "data";
	public static final String MODE       = "mode";



	public static final String STATE      = "state";
	public static final String FLAG       = "flag";
	public static final String HAPPINESS  = "happiness";
	public static final String DIRTINESS  = "dirtiness";
	public static final String KIDS       = "kids";


	public static final String GREEN       = "Green";
	public static final String YELLOW      = "Yellow";
	public static final String RED         = "Red";


	/**
	 * @return of type Constants
	 * Constructor function
	 * @since Oct 27, 2013 
	 * @author rajeshcp
	 */
	private Constants() {
	}

}
